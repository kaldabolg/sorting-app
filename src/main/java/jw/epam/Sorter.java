package jw.epam;

import java.util.Arrays;

/**
 * Class that provides a static method to sort an array of integers
 */
public class Sorter {
    /**
     * Sorts an array of integers
     * @param ints
     * @throws IllegalArgumentException
     */
    public static void sort(int... ints) throws IllegalArgumentException {
        if (ints == null)
            throw new IllegalArgumentException("illegal sort() method argument: null");
        if (ints.length > 10)
            throw new IllegalArgumentException("sort() method takes at most 10 integer arguments");
        if (ints.length < 2)
            return;
        Arrays.sort(ints);
    }
}
