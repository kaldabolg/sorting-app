package jw.epam;

import java.util.Arrays;

/**
 * Main class that starts the application
 */
public class Main {
    /**
     * The main method. Arguments need to be integer-parsable
     * @param args
     */
    public static void main(String[] args) {
        try {
            Sorter sorter = new Sorter();
            int[] arguments = Arrays.stream(args).mapToInt(Integer::parseInt).toArray();
            sorter.sort(arguments);
            for (int arg :
                    arguments) {
                System.out.print(arg + " ");
            }
        } catch (NumberFormatException e) {
            System.out.println("Application accepts integers only.");
            return;
        } catch (IllegalArgumentException e) {
            System.out.println("Application can sort at most 10 integers.");
            return;
        }
    }
}