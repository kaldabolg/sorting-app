package jw.epam;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SorterTooManyParamsTest {
    private final int[] expected;
    private final int[] data;

    public SorterTooManyParamsTest(int[] expected, int[] data) {
        this.expected = expected;
        this.data = data;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new int[]{Integer.MIN_VALUE, -15, 2, 9, 11, 18, 18, 2, 22, 2142, Integer.MAX_VALUE}, new int[]{11, 2, 18, 2, Integer.MAX_VALUE, 2142, -15, 9, Integer.MIN_VALUE, 22, 18}}
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSort() {
        Sorter.sort(data);
    }
}
