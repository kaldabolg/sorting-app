package jw.epam;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class SorterOneParamTest {
    private final int[] data;
    private final int[] expected;

    public SorterOneParamTest(int[] expected, int[] data) {
        this.expected = expected;
        this.data = data;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new int[]{-2142}, new int[]{-2142}},
                {new int[]{0}, new int[]{0}},
                {new int[]{21}, new int[]{21}},
                {new int[]{9921}, new int[]{9921}}
        });
    }

    @Test
    public void testSort() {
        Sorter.sort(data);
        assertArrayEquals(expected, data);
    }
}
