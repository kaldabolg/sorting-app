package jw.epam;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SorterFewParamsTest {
    private final int[] expected;
    private final int[] data;


    public SorterFewParamsTest(int[] expected, int[] data) {
        this.expected = expected;
        this.data = data;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new int[]{-2, 3, 5, 77}, new int[]{5, -2, 77, 3}},
                {new int[]{-92, -91}, new int[]{-91, -92}},
                {new int[]{0, 0, 0, 0, 0}, new int[]{0, 0, 0, 0, 0}},
                {new int[]{-15, 2, 9, 11, 18, 2142}, new int[]{11, 2, 18, 2142, -15, 9}}
        });
    }

    @Test
    public void testSort() {
        Sorter.sort(data);
        assertArrayEquals(expected, data);
    }
}
