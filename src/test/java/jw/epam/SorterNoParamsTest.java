package jw.epam;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class SorterNoParamsTest {
    private final int[] data;

    public SorterNoParamsTest(int[] data) {
        this.data = data;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new int[]{}}
        });
    }

    @Test
    public void testSort() {
        Sorter.sort(data);
        assertEquals(0, data.length);
    }
}
