package jw.epam;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

public class SorterNullParamTest {

    @Test(expected = IllegalArgumentException.class)
    public void testSort() {
        Sorter.sort(null);
    }
}
